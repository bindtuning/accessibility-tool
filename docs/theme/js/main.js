/**
Global lets
 */
let spanIDs = [];
let lastScrollTop = 0;
let $window = jQuery(window);
let $header = "";
let $headerParent = "";
let $footer = "";
let $sideNavWrapper = "";
let $sideNav = "";
let $size = {};
let $topMargin = 30;
let path = {
  partials: "theme/partial/"
};

let loadingHTML = '<div id="bt-loading"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>';

let flags = {
  "header": false,
  "footer": false,
  "extrafiles": true,
  "mobile": false,
  "openSideBar": true //The SideBar is open on *False*
}

// Initialize
INIT();

/**
 * Methods
 */
function INIT() {
  // Add new BindTuning Color Loading
  jQuery(loadingHTML).appendTo("body");

  // Detect if is Mobile
  detectmob();

  // When all is on Page
  jQuery(document).ready(function () {

    // Replace all Button Styles on Page
    replaceAllButtonClasses();

  });
  // Verify if all is injected
  let checkExist = setInterval(function () {
    if (flags.header && flags.footer && flags.extrafiles) {
      cleanHTTPflags();

      // Button Action on click in opensideBar (BT-OpenSideBar -> RTD )
      jQuery("a.BT-OpenSideBar").on("click", function () {
        openCloseSideBarMobile();
      });

      // Allow hover on BindTuning Navbar
      jQuery("div.navbar-item.has-dropdown").hover(function () {
        jQuery(this).addClass('is-active');
      }).mouseleave(function () {
        jQuery(this).removeClass('is-active');
      });

      // On Click Get Started
      jQuery('a.getstarted-click').on('click', function () {
        const email = document.getElementById('getstarted-input-header').value;
        const baseURL = 'https://app.bindtuning.com/getstarted';

        window.location.href = email !== '' ? baseURL + "?email=" + email : baseURL;
      });

      //Init Styles of SideBar (Read the Docs)
      initSideBarStyles();

      // Hide BT-Loading
      jQuery("#bt-loading").css("display", "none");
      
      clearInterval(checkExist);

    }
  }, 500);

  // Read Header -> BindTuning Site (header.html)
  jQuery.get("/en/latest/" + path.partials + "header.html", function (data) {
    jQuery(".wy-grid-for-nav").append(data);

    setTimeout(function () {
      flags.header = true;
    }, 250);
  });

  // Read Footer -> BindTuning Site (footer.html)     
  jQuery.get("/en/latest/" + path.partials + "footer.html", function (data) {
    jQuery(".wy-grid-for-nav").append(data);

    setTimeout(function () {
      flags.footer = true;
    }, 250);
  });

}



//Init Styles to Device
function initSideBarStyles() {

  $header = jQuery('header.header nav.navbar');
  $headerParent = jQuery('header.header');
  $footer = jQuery('footer.has-background-beige');
  $sideNavWrapper = jQuery('div.wy-menu.wy-menu-vertical');
  $sideNav = jQuery('nav.wy-nav-side');

  $size["window"] = $window.height();
  $size["header"] = $headerParent.outerHeight();
  $size["footer"] = $footer.outerHeight();

  jQuery('nav.stickynav').css("top", $size.header);

  // Prevent resize 
  jQuery('body').css('overflow', 'auto');
  jQuery('.wy-nav-content-wrap').css('opacity', 1);

  // Hide SideBar
  jQuery('nav.wy-nav-side').css("display", "none");

  // RTD Theme Footer Hide
  jQuery('.wy-nav-content footer').css("color", "#fff").children().not('div.rst-footer-buttons').hide();

  // Validate if is mobile
  detectmob();

  // Choose Styles if has a mobile or a desktop device
  if (flags.mobile) {
    initMobileSideBar();
  } else {
    initDesktopSideBar();
  }

}

//Init SideBar for Desktop Device
function initDesktopSideBar() {

  jQuery('.BT-OpenSideBar').css("display", "none");
  jQuery('nav.wy-nav-side').css("display", "block");
  jQuery('nav.wy-nav-side').css("left", ($window.width() - $header.width()) / 2 + "px");
  jQuery('.wy-nav-content .rst-content').css("margin", "25px " + ($window.width() - $header.width()) / 2 + "px 0 " + (($window.width() - $header.width()) / 2 + $sideNav.width()) + "px");
  //jQuery('.wy-nav-side.stickynav').css('height', $size.window - ($size.header + ($topMargin)));
  jQuery('nav.wy-nav-side').removeClass('BT-MobileMenu');

  jQuery('body').scrollTop(0);
}


//Init SideBar for Mobile Device
function initMobileSideBar() {

  let calcSideNavHeight = $size.window - $size.header;
  //Show Button to Mobile
  jQuery('.BT-OpenSideBar').css("display", "inherit");
  jQuery('.BT-OpenSideBar').removeClass('is-active');
  jQuery("nav.wy-nav-side").css("left", "-330px")

  //Add class to navbar
  jQuery('nav.wy-nav-side').addClass('BT-MobileMenu');
  jQuery('div.wy-menu.wy-menu-vertical, nav.wy-nav-side').css("height", calcSideNavHeight + "px");
  if (flags.mobile) {
    jQuery('.wy-nav-content .rst-content').css("margin", "81px 0 40px");
  }else{
    jQuery('.wy-nav-content .rst-content').css("margin", "81px 15px 40px");
  }

  flags.openSideBar = true;
}


// FUNCTIONS

/**
 * Replace the value of class buttons to new buttons
 */
function replaceAllButtonClasses() {
  jQuery(".btn").addClass('button is-primary is-outlined');
  jQuery(".btn.float-right").removeClass('is-outlined');
  jQuery(".btn").removeClass('btn btn-neutral');
}

/**
 * Validate if is mobile
 */
function detectmob() {
  if (window.innerWidth < 769) {
    flags.mobile = true;
  } else {
    flags.mobile = false;
  }
}

/**
 * Add class to RTD SideBar Menu to change to a Collapsable Menu
 */
function menuCollapse() {

  //Remove element on Introduction <a></a> on BindTuning Account Menu
  jQuery('li.toctree-l1 ul.subnav li ul').eq(0).remove();

  // lets
  let elementBtn = "span.caption-text"
  let elementUl = "li.toctree-l1 ul.subnav"
  let spansMenu = jQuery(elementBtn);
  let ulMenu = jQuery(elementUl);

  //Fill all Buttons on Menu 
  for (let x = 0; x < spansMenu.length; x++) {
    let spanID = jQuery(elementBtn)[x].textContent.toLowerCase().replace(" ", "_");
    jQuery(elementBtn).eq(x).attr("data-target", "#" + spanID);
    jQuery(elementBtn).eq(x).attr("data-toggle", "collapse");
    jQuery(elementBtn).eq(x).attr("aria-expanded", "true");
    jQuery(elementBtn).eq(x).attr("aria-controls", spanID);
    jQuery(elementBtn).eq(x).addClass("text-bold");

    spanIDs.push(spanID);

    // At the end Fill Respectives subMenus
    if (x === (spansMenu.length - 1) && spanIDs.length === spansMenu.length) {

      for (let z = 0; z < ulMenu.length; z++) {
        jQuery(elementUl).eq(z).addClass("collapse in");
        jQuery(this).eq(z).css('display', 'inherit');

        jQuery(elementUl).eq(z).addClass("bt-collapse");
        jQuery(elementUl).eq(z).attr("id", spanIDs[z]);
      }

    }
  }
}

/**
 * Clean HTTP Flags (when all is loaded)
 */
function cleanHTTPflags() {
  flags.header = false;
  flags.footer = false;
  flags.extrafiles = false;
}

/**
 * Open or Close SideBar Mobile
 */
function openCloseSideBarMobile() {

  const barWidth = jQuery("nav.wy-nav-side").width();

  if (flags.openSideBar) {
    jQuery('nav.wy-nav-side').css("display", "block");
    setTimeout(function () {
      jQuery('.BT-OpenSideBar').addClass('is-active');
      jQuery("nav.wy-nav-side").css("left", "0px");
      jQuery('body').css('overflow', 'hidden');
      jQuery('.wy-nav-content-wrap').css('opacity', .1);
      flags.openSideBar = false;
    }, 250);
  } else {
    jQuery('.BT-OpenSideBar').removeClass('is-active');
    jQuery("nav.wy-nav-side").css("left", -barWidth + "px");
    jQuery('body').css('overflow', 'auto');
    jQuery('.wy-nav-content-wrap').css('opacity', 1);
    flags.openSideBar = true;
  }
}

// LISTNERS

/**
 * Hide content of collapse
 */
jQuery('.bt-collapse').on('hidden.bs.collapse', function () {
  jQuery(this).css('display', 'none');
});

/**
 * Show content of collapse
 */
jQuery('.bt-collapse').on('show.bs.collapse', function () {
  jQuery(this).css('display', 'inherit');
});


/**
 * On Resize the page "reload" styles
 */
jQuery(window).on('resize', function () {
  initSideBarStyles();
});

/**
 * Scroll event for sticky sidenav
 */
jQuery('body').on('scroll', function () {

  let bodyScroll = jQuery(this).scrollTop();
  if (!flags.mobile) {

    if (bodyScroll > lastScrollTop) {
      // downscroll code
      if (bodyScroll + (2 * $topMargin) + $sideNav.height() > jQuery(document).height() - $size.footer) {
        $sideNav.css({
          "position": "absolute",
          "top": jQuery(document).height() - ($size.footer + $topMargin + $sideNav.height())
        });
      } else if (bodyScroll > $size.header - $topMargin) {
        $sideNav.css({
          "position": "fixed",
          "top": $topMargin
        });
      } else {
        $sideNav.css({
          "position": "absolute",
          "top": $size.header
        });
      }
    } else {

      if (bodyScroll >= 0 && bodyScroll <= $size.header) {
        $sideNav.css({
          "position": "absolute",
          "top": $size.header
        });
      } else if (bodyScroll + (2 * $topMargin) + $sideNav.height() < jQuery(document).height() - $size.footer) {
        $sideNav.css({
          "position": "fixed",
          "top": $topMargin
        });
      } else {
        $sideNav.css({
          "position": "absolute",
          "top": jQuery(document).height() - ($size.footer + $topMargin + $sideNav.height())
        });
      }
    }
  }
  lastScrollTop = bodyScroll;
});