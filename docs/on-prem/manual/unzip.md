The tools are provided in 2 different packages, all following Microsoft guidelines:

For **Classic Experience**: 

 - ***Sandbox Solution***, for Sandbox environments (*.wsp).

For **Modern Experience**: 

 - ***Modern Solution***, for Modern SharePoint (*.sppkg).

After unzipping your tool package, you will find two folders,**modern** and **classic**, as well as an installer **installer_BTAccessibilityTool.ps1**. 
Depending on the SharePoint configuration you may have to install both packages. 

![zip.png](../../images/zip.png)

-------------

**Classic Solution** (Classic SharePoint) 📁

── *BTAccessibilityTool_x.x.x.x*.wsp

-------------

**Modern Solution** (Modern SharePoint) 📁

── *BTAccessibilityTool_x.x.x.x*.sppkg

-----------

**Installer** (For both the **Classic** and **Modern** experiences) 📁

── *installer_BTAcessibilityTool*.ps1
