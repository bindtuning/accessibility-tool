To **manually install** your Accessibility Tool, you'll have to **download** the corresponding package.

1. Access your account at <a href="https://bindtuning.com" target="_blank">BindTuning</a>;
2. Go to the **Accessibility** feature.
3. Mouse hover the tool and click **More Details** to open the tool details page;

    ![select-tool.png](../../images/select-tool.png)

4. Last but not least, click on **Download**.

    ![download-tool](../../images/download-tool.png)
