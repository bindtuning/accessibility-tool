On **Classic environments**, BindTuning offers two distinct deployment methods. 

- **Upload the Solution**: Comprises the upload of the solution to your **Solutions gallery** and subsequent activation.

- **Installation script**: Performs the necessary steps (deployment and activation) automatically.

**Note:** Only one of the methods need to performed.

---
### Upload the Solution

Inside your tool's *classic* folder you will find a **.wsp** file which you will be using for installing the tool. 


1. Open your SharePoint Site;
2. Click on **Settings** ⚙️ and then on **Site settings**;
3. Under "Web Designer Galleries", click on **Solutions**;
	
	**Note:** If you're not working on your root site this option will not appear. 
	
4. Click on **Upload Solution**;
5. Upload the *BTAccessibilityTool_x.x.x.x.wsp* file. You can find the file by opening your tool's pack and the *classic* folder;
6. Click OK; 
7. **Click on "Activate"** to activate the tool.
	
The tool has been installed! ✅ 

---

### Installation script

Inside the tool package, you'll find the  ***installer_BTAccessibilityTool.ps1*** PowerShell script, which will automatically **deploy** and **apply** the solution to the required site collection. 

1. Right-click on the file and select the option **Run with PowerShell**; 

1. If prompted, download the required **Cmdlets**, in order to proceed with the installation; 

	![download-cmdlets.png](../../../images/download-cmdlets.png)

1. Select the **Classic** option and, hit **Enter**;

	![powershell-install-classic](../../../images/powershell-install-classic.png)

1. Select the option **Install/Update Accessibility Tool and activate on the site collection** and hit **Enter**; 

	![powershell-apply-classic.png](../../../images/powershell-apply-classic.png)

1. Input the corresponding site collection where the product is to be installed and hit **Enter**; 

	![powershell-sitecollection-classic.png](../../../images/powershell-sitecollection-classic.png)

1. If prompted, input your **Farm Credentials**; 

1. The process will run automatically.

	![powershell-finish-classic.png](../../../images/powershell-finish-classic.png)

The tool has been installed! ✅
