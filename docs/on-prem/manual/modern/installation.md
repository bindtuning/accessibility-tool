1. Right click the ***installer_BTAccessibilityTool.ps1*** file and select **Run with PowerShell** (run as administrator);

1. If prompted, download the required **Cmdlets**, in order to proceed with the installation; 

	![download-cmdlets.png](../../../images/download-cmdlets.png)


---
<a name="install"></a>
### Installing the Accessibility Tool on a site collection

1. If you're not running from the previous section right click the ***installer_BTAccessibilityTool.ps1*** file and select **Run with PowerShell**;

1. Select the **Modern** option and, hit **Enter**;

	![powershell-install-modern](../../../images/powershell-install-modern.png)
	
1. Select the option **Install/Update Accessibility Tool and activate on the site collection** and hit **Enter**; 

	![powershell-install-update-sitecollection-modern.png](../../../images/powershell-install-update-sitecollection-modern.png)

1. Input the site collection where the tool is to be installed and hit **Enter**;

1. If prompted, input your **Farm Credentials**; 

1. The process will run automatically.

	![powershell-finish-classic.png](../../../images/powershell-finish-classic.png)

Tool installed on the Site! ✅ 

---

### Apply the Accessibility Tool

If you already have the tool available on your **tenant app catalog**, you can  apply it to any Modern SharePoint Site collection you want.

1. If you're not running form the previous section right click the ***installer_BTAccessibilityTool.ps1*** file and select **Run with PowerShell**;

1. Select the option **Activate Accessibility Tool to a site collection**;
	![powershell-apply-modern.png](../../../images/powershell-apply-modern.png)
	
1. Insert the site collection where the tool should be applied;

1. If prompted, input your **Farm Credentials**. 

Tool applied to the Site! ✅ 