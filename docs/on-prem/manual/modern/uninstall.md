### Deactivate the tool

1. Right click the ***installer_BTAccessibilityTool.ps1*** file and select **Run with PowerShell**;

1. Select the option **Modern version** and hit **Enter**;

1. Select the option **Dectivate Accessibility Tool**;

    ![powershell-deactivate-modern.png](../../../images/powershell-deactivate-modern.png)

1. Select the option **from site collection**;

    ![powershell-deactivate-scope-modern.png](../../../images/powershell-deactivate-scope-modern.png)

1. If prompted, input your **Farm Credentials**; 

Tool deactivated! ✅ 

---
### Uninstall tool

1. Right click the ***installer_BTAccessibilityTool.ps1*** file and select **Run with PowerShell**;

1. Select the option **Modern version** and hit **Enter**;

1. Select the option **Uninstall Accessibility Tool**;

    ![powershell-uninstall-modern.png](../../../images/powershell-uninstall-modern.png)

1. Input the site collection you want to uninstall the tool from;

1. If prompted, input your **Farm Credentials**; 
    
Tool uninstalled! ✅