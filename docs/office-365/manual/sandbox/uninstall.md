The BindTuning Accessibility Tool can be uninstalled following two distinct methods: 

- **Manual** Uninstallation; 
- **Automatic** Uninstallation.
---
### Manual Uninstallation 

<a name="deactivateanddeletethetool"></a>
#### 1. Deactivate and delete the tool on SharePoint:

1. On your root site open the *Settings* menu and click on *Site Settings*;

2. Under *Web Design Galleries*, click on *Solutions*;

3. Select *BTAccessibilityTool_x.x.x.x*.wsp file, and click on *Deactivate*;

4. Select *BTAccessibilityTool_x.x.x.x*.wsp file again, and click on *Delete*.

<a name="finishingoff"></a>
### 2. Finishing off:
Only a few more steps to completly remove and uninstall your tool. 

1. Open your site with **SharePoint designer** and open **All Files**;
1.  Open your **Style Library**.
1. Delete the folder **BTAccessibilityTool**. 

**Success**: You have now successfully removed and uninstalled your tool.

--- 
### Automatic Uninstallation

Inside the tool package, you'll find the  ***installer_BTAccessibilityTool.ps1*** PowerShell script, which will automatically **remove** and **uninstall** the solution in the required site collection. 

1. Right-click on the file and select the option **Run with PowerShell**; 

1. If prompted, download the required **Cmdlets**, in order to proceed with the installation; 

	![download-cmdlets.png](../../../images/download-cmdlets.png)

1. Select the **Classic** option and, hit **Enter**;

	![powershell-install-classic](../../../images/powershell-install-classic.png)

1. Select the option **Uninstall Accessibility Tool** and hit **Enter**; 

	![powershell-uninstall-classic.png](../../../images/powershell-uninstall-classic.png)

1. Input the corresponding site collection where the product is to be installed and hit **Enter**; 

	![powershell-sitecollection-classic.png](../../../images/powershell-sitecollection-uninstall-classic.png)

1. If prompted, input your **Office 365 credentials**; 

1. The process will run automatically.

The tool has been uninstalled! ✅