### Set up the pre-installation environment 
Before moving on to installing your tool, there is a couple of things you will need to do. 

- You will need to activate the **SharePoint Server Publishing Infrastructure** feature.
- Deactivate the **Minimal Download Strategy** feature, that usually appears activated by default in these type of sites.
- Lastly, activate the **SharePoint Server Publishing** feature.

<p class="alert alert-success">To activate SharePoint Publishing Features you will have to be an Admin or to have been granted permissions from your Admin.</p>

1. Open the settings menu and click on **Site settings**; 
2. Under **Site Collection Administration**, click on **Site collection features**; 
	
	**Note:** If you're seeing **Go to top level site settings** instead of **Site collection features** under **Site Collection Administration**, click on **Go to top level site settings** and then **Site collection features**.</p>
	

3. Search for *SharePoint Server Publishing Infrastructure* and click on **Activate**. *SharePoint Server Publishing Infrastructure* activated. ✅ On to deactivating the *Minimal Download Strategy*.
4. On your root site, open the settings menu and click on **Site settings**;
5. Under **Site Actions**, click on **Manage Site Features**;
6. Search for *Minimal Download Strategy* and click on **Deactivate**. *Minimal Download Strategy* deactivated. ✅ 
7. In the same list, search for *SharePoint Server Publishing* and click on **Activate**. *SharePoint Server Publishing* activated. ✅