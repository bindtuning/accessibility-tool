Before upgrading your tool's version, you will need to request the new version at BindTuning.com and follow the steps on the <a href="https://bindtuning-accessibility-tool.readthedocs.io/en/latest/bindtuning/update/" target="_blank">next link</a>.

---
### Manual Update

nside your tool's *classic* folder you will find a **.wsp** file which you will be using for updating the tool. 


1. Open your SharePoint Site;
2. Click on **Settings** ⚙️ and then on **Site settings**;
3. Under "Web Designer Galleries", click on **Solutions**;
	
	**Note:** If you're not working on your root site this option will not appear. 

4. Select the *BTAccessibilityTool_x.x.x.x.wsp* old file, and choose **Deactivate**; 

5. Click on **Upload Solution**;

5. Upload the *BTAccessibilityTool_x.x.x.x.wsp* new file. You can find the file by opening your tool's pack and the *classic* folder;

6. Click OK;

7. **Click on "Activate"** to activate the tool.

Version upgraded! ✅

---
### Automated update

The automated installation, using the ***installer_BTAccessibilityTool.ps1***, will automatically replace and upgrade your tool's version.

You can find the instructions for installing the tool <a href="https://bindtuning-accessibility-tool.readthedocs.io/en/latest/office-365/manual/sandbox/installation/" target="_blank">here</a>.

Version upgraded! ✅