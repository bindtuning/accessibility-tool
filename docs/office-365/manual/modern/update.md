The automated installation, using the ***installer_BTAccessibilityTool.ps1***, will automatically **replace** and **upgrade**, and **apply** your tool to the chosen SharePoint site version.

To **update/install** our Accessibility Tool, follow the link <a href="https://bindtuning-accessibility-tool.readthedocs.io/en/latest/office-365/manual/modern/installation/">here</a>. 