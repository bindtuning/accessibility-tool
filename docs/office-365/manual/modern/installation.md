
1. Right click the ***installer_BTAccessibilityTool.ps1*** file and select **Run with PowerShell** (run as administrator);

1. If prompted, download the required **Cmdlets**, in order to proceed with the installation; 

	![download-cmdlets.png](../../../images/download-cmdlets.png)


---
<a name="install"></a>
### Installing the Accessibility Tool on a site collection

1. If you're not running from the previous section right click the ***installer_BTAccessibilityTool.ps1*** file and select **Run with PowerShell**;

1. Select the **Modern** option and, hit **Enter**;

	![powershell-install-modern](../../../images/powershell-install-modern.png)
	
1. Select the option **Install/Update Accessibility Tool and activate on the site collection** and hit **Enter**; 

	![powershell-install-update-sitecollection-modern.png](../../../images/powershell-install-update-sitecollection-modern.png)

1. Choose the installation scope for the Accessibility Tool; 

	- **Tenant App Catalog:** Deploys the solution globally, but only applies the Accessibility Tool to the specified site collection. 
	- **Site Collection App Catalog:** Deploys and applies the solution to the specified site collection.

	![powershell-sitecollection-classic.png](../../../images/powershell-sitecollection-modern.png)

1. If prompted, input your **Office 365 credentials**;

	**Note:** If the creation of the **site collection app catalog** fails, you may have to create it manually. To do so, simply follow the steps in this <a href = "https://support.bindtuning.com/hc/en-us/articles/360027696532-What-is-and-How-to-create-an-App-Catalog-Tenant-vs-Site-Collection-App-Catalog">article</a> and resume the installation.

1. The process will run automatically.

	![powershell-finish-classic.png](../../../images/powershell-finish-classic.png)



Tool installed on the Site! ✅ 

---

<a name="install"></a>
### Installing the Accessibility Tool on a HUB site 

<p class="alert alert-info"> What is a SharePoint hub site? <br>SharePoint hub sites help you meet the needs of your organization by connecting and organizing sites based on project, department, division, region, etc. making it easier to... <a href="https://support.office.com/en-us/article/what-is-a-sharepoint-hub-site-fe26ae84-14b7-45b6-a6d1-948b3966427f" target="_blank">read more</a></p>

1. If you're not running from the previous section Right click the ***installer_BTAccessibilityTool.ps1*** file and select **Run with PowerShell**;

1. Select the option **Install/Update Accessibility Tool and activate to a HUB and associated sites**;
	
	![powershell-install-hub-modern.png](../../../images/powershell-install-hub-modern.png)
	
1. Insert the HUB link where the tool should be applied;

	![powershell-hub-url-modern](../../../images/powershell-hub-url-modern.png)

1. If prompted, input your **Office 365 credentials**; 


Tool installed on all sites of the selected HUB! ✅ 

---

<a name="apply"></a>
### Apply the tool

If you already have the tool available on your **tenant app catalog**, you can  apply it to any Modern SharePoint Site collection you want.

1. If you're not running form the previous section right click the ***installer_BTAccessibilityTool.ps1*** file and select **Run with PowerShell**;

1. Select the option **Activate Accessibility Tool to a site collection**;
	![powershell-apply-modern.png](../../../images/powershell-apply-modern.png)
	
1. Insert the site collection where the tool should be applied;

1. If prompted, input your **Office 365 credentials**; 

Tool applied to the Site! ✅ 