The BindTuning app allows for the easy deployment of our accessibility tools on Office 365.

<p class="alert alert-success">This installation method is only valid for <strong>Office 365 (SharePoint Online)</strong>.</p>

1. Login to your BindTuning account. 
    <p>
1. Navigate to the **Accessibility** tab and click on **tools Gallery**
    <p>

    ![accessibility-tab](../../../images/accessibility-tab.png)
     
1. Mouse over the selected tool and click on **More Details**
    <p>
     
1. Proceed with either:
    * **Install**: Installs the Accessibility Tool automatically;
    * **Download**: To proceed with the Manual installation of the tool; 

    <p class="alert alert-success"> To proceed with the Manual Installation, follow this <a href="https://bindtuning-accessibility-tool.readthedocs.io/en/latest/office-365/manual/download/">link</a>.</p>

    ![accessibility-install](../../../images/install-accessibility-tool.png)
    

1. Select the **SharePoint experience** you want to deploy to, as well as correlative **installation scope**:
    
    - **Classic:** Deploys the tool on **Classic SharePoint**, being available at the site collection level;
    - **Modern:** Deploys the tool on **Modern SharePoint**, being available at either **site collection** or **tenant** level.

    ![credentials-app.png](../../../images/installation-scopes.png)

    **Note:** Despite adding the solution to your **Tenant App Catalog**, only the **site collection** added in **step 5** will reflect the tool. From there, you can choose to either apply the tool to a selected number of site collections or to every site collection within your tenant:<p>
    - To apply the tool to **selected** site collections, follow the steps <a href="https://bindtuning-accessibility-tool.readthedocs.io/en/latest/office-365/manual/modern/installation/">here</a>; <p>
    - To apply the tool to **all** site collections across your tenancy, follow the steps <a href="https://support.bindtuning.com/hc/en-us/articles/360022325092-Activate-Modern-tool-for-the-tenant">here</a>.

1. Verify all the provided information and click **Confirm** 
    <p>

    ![confirm-installation](../../../images/confirm-installation.png)
   
1. The installation will proceed in the background. To review it's progress click on **Recent Activity** (graph icon).
    <p>

    ![check-installation-progress](../../../images/check-installation-progress.png)
<p>

After the installation has been completed, an alert will appear, informing you of its status. ✅


<!-- ---
### Apply the tool

To apply the tool, follow the instructions [here](../../../docs/automated/apply.md). -->