Before upgrading your tool's version, you will need to request the new version at <a href="https://bindtuning.com">BindTuning</a>, and following the steps below.

---
### Update your Accessibility Tool 

1. Login to your BindTuning account; 
    
1. Navigate to the **Accessibility** tab and click on **tools Gallery**;
    

    ![accessibility-tab](../images/accessibility-tab.png)

    **Note:** You can check if there's an update pending by verifying if a **red circle icon** shows up on the top-right corner of your **Accessibility Tool** tile.

     ![update-available](../images/update-available.png) 
     
1. Mouse over the selected tool and click on the **ellipsis** button; 

1. Select the option **Update now**.

     ![update-now](../images/update-now.png)

The update process will start. ✅

---
### Install the tool 

After proceeding with the update of the tool, you will need to proceed with it's re-installation. To do this, please follow the steps below: 

1. For **Office 365**: 
    
    - The **automated installation** can be performed by following this <a href="https://bindtuning-accessibility-tool.readthedocs.io/en/latest/office-365/automated/app/installation/">link</a>; 
    - The **manual installation** can be performed by following this <a href="https://bindtuning-accessibility-tool.readthedocs.io/en/latest/office-365/manual/download/">link</a>. 

2. For SharePoint On-Premises: 

    - The **manual installation** can be performed by following this <a href="https://bindtuning-accessibility-tool.readthedocs.io/en/latest/on-prem/manual/download/">link</a>. 