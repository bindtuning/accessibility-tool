BindTuning provides you with the flexibility to install our products the way you want to, regardless of technical knowledge or experience. 
<br>

Following this, Bindtuning offers two main installation processes:

- **Automated** installation;
- **Manual** installation. 

**Note:** We recommend the **Automated** installation, due to its easy deployment process. 

---
### Automated Installation (SharePoint Online)

The **Automated** installation process makes the deployment of BindTuning's products as easy as a simple click. Depending on your needs, you'll be able to choose how to proceed with the installation.
<br>

#### BindTuning app

The **BindTuning app** offers the possibility to deploy, your products to **SharePoint Online**, using our dedicated website. 
<br>


<a href="https://bindtuning-accessibility-tool.readthedocs.io/en/latest/office-365/automated/app/installation/">Here</a> you'll learn how to install the products using the **BindTuning app**.


---

### Manual installation (SharePoint Online and SharePoint On-Premises)

The **Manual** installation process allows for more granular control on the installation with an added complexity layer, not present in the **Automated** installation. 

The **Manual** installation is available for **SharePoint Online** and **SharePoint On-Premises**, and will depend on the particular experience present on your site:

1. For **Office 365**: 
    
    - The **manual installation** can be performed by following this <a href="https://bindtuning-accessibility-tool.readthedocs.io/en/latest/office-365/manual/download/">link</a>. 

2. For SharePoint On-Premises: 

    - The **manual installation** can be performed by following this <a href="https://bindtuning-accessibility-tool.readthedocs.io/en/latest/on-prem/manual/download/">link</a>. 





